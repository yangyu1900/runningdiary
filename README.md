# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a naive running app for Android devices. Apart from keeping track of basic running stats, the visualization of running routes and music player features are also added. 

This is the first version which stores data on phone's local database.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

This project is developed using Android Studio. So import the project into Android Studio should be able to run the app.

It is recommended to use 6.0 or higher version of Android Virtual Device, though it also works on android 4.0+ devices.

Dependencies are defined in gradle files which would be imported automatically.

No database configuration is required since this version simply keeps data in phone's sqlite database.

A generated apk is also available in the APK folder.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Repo owner